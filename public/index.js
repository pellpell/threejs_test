import * as THREE from 'three';
import { OrbitControls } from 'https://unpkg.com/three@0.142.0/examples/jsm/controls/OrbitControls.js';
import { GLTFLoader } from 'https://unpkg.com/three@0.142.0/examples/jsm/loaders/GLTFLoader.js';

// 参考: https://web-creates.com/code/blender-threejs/

window.addEventListener('DOMContentLoaded', init);

function init() {
  // レンダラーを作成
  const renderer = new THREE.WebGLRenderer({
    canvas: document.querySelector('#myCanvas'),
    alpha: true,
  });
  // ウィンドウサイズ設定
  const width = document.getElementById('main_canvas').getBoundingClientRect().width;
  const height = document.getElementById('main_canvas').getBoundingClientRect().height;
  renderer.setPixelRatio(1);
  renderer.setSize(width, height);

  // シーンを作成
  const scene = new THREE.Scene();
  const bgColor = 0xf7899b;
  scene.background = new THREE.Color(bgColor); // 背景色

  // カメラを作成
  const camera = new THREE.PerspectiveCamera(45, width / height, 1, 10000);
  camera.position.set(0, 400, -1000);

  const controls = new OrbitControls(camera, renderer.domElement);

  // Load GLTF or GLB
  const loader = new GLTFLoader();
  const url = 'assets/3d/desk_chair.glb';

  // window size
  const w_height = window.innerHeight;

  let model = null;
  loader.load(
    url,
    function (gltf) {
      model = gltf.scene;
      // model.name = "desk_chair";
      // model.scale.set(100.0, 100.0, 100.0);
      const scale = 160.0;
      model.scale.set(scale, scale, scale);
      model.position.set(0, 0, 0);
      scene.add(gltf.scene);
    },
    // function (error) {
    //   console.log('An error happened');
    //   console.log(error);
    // }
  );
  renderer.gammaOutput = true;


  // 平行光源
  const light = new THREE.DirectionalLight(0xFFFFFF);
  light.intensity = 1; // 光の強さ
  light.position.set(3, 10, 1);
  scene.add(light);


  //環境光源
  const ambient = new THREE.AmbientLight(0xf8f8ff, 0.7);
  scene.add(ambient); //シーンにアンビエントライトを追加

  // 初回実行
  tick();

  function tick() {
    controls.update();

    if (model != null) {
      // console.log(model);
    }
    renderer.render(scene, camera);
    requestAnimationFrame(tick);
  }
}
