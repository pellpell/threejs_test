# Threejs_test

Three.jsでBlenderの3Dモデルを読み込むテストページを作成しました.

参考サイト:

- <a href="https://youtu.be/S6aAvxUx2ko" target="_blank" rel="noopener">3Dモデル</a>
- <a href="https://web-creates.com/code/blender-threejs/" target="_blank" rel="noopener">Webページ</a>
